const app = require("application");
const http = require("http");
const utils = require("utils/utils");
const frame = require("ui/frame");



function StartApp() {
	this.activity = null;
};


StartApp.prototype.init = function(startAppId) {
	com.startapp.android.publish.adsCommon.StartAppSDK.init(app.android.context, startAppId, true);
}


StartApp.prototype.showInterstitials = function() {
	com.startapp.android.publish.adsCommon.StartAppAd.showAd(app.android.context);
}


StartApp.prototype._getActivity = function() {
  if (this.activity === null) {
    this.activity = app.android.foregroundActivity;      
  }
  return this.activity;
};


StartApp.prototype.showBanner = function() {
	const relativeLayoutParams = new android.widget.RelativeLayout.LayoutParams(
		android.widget.RelativeLayout.LayoutParams.MATCH_PARENT,
		android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);
	

	relativeLayoutParams.addRule(android.widget.RelativeLayout.ALIGN_PARENT_BOTTOM);
		
	relativeLayoutParams.addRule(android.widget.RelativeLayout.CENTER_HORIZONTAL);
	
    const adViewLayout = new android.widget.RelativeLayout(this._getActivity());
    adViewLayout.addView(new com.startapp.android.publish.ads.banner.Banner(app.android.context), relativeLayoutParams);
	
	
	const relativeLayoutParamsOuter = new android.widget.RelativeLayout.LayoutParams(
		android.widget.RelativeLayout.LayoutParams.MATCH_PARENT,
		android.widget.RelativeLayout.LayoutParams.MATCH_PARENT);
		
	setTimeout(function() {
		frame.topmost().currentPage.android.getParent().addView(adViewLayout, relativeLayoutParamsOuter);
	}, 0);
}


module.exports = StartApp;

