# NativeScript StartApp plugin


For now only working with android. (Javascript only)
Content api:

  - Interstitial
  - Banner

# Usage
```
const StartApp = require('nativescript-startapp');
global.startApp = new StartApp();




//Init
global.startApp.init('APP_ID');


global.startApp.showInterstitials();

global.startApp.showBanner();

```
